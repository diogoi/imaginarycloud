Rails.application.routes.draw do
  resources :packages
  resources :hotels
  root :to => "welcome#show"
end
