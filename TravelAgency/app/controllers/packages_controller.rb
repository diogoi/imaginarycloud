class PackagesController < ApplicationController

  before_action :require_login

  def index
  end


  def new
    @package = Package.new
    @hotels = Hotel.all
  end


  def create
    @package = Package.new(package_params)

    if @package.save
      redirect_to '/'
    else
      @errors = @package.errors.full_messages
      render :new
    end
 
  end

  private

  def package_params
    params.require(:package).permit(:destination, :departure_date, :arrival_date, :hotels_id)
  end
end
