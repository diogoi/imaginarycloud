class HotelsController < ApplicationController
  before_action :require_login

  def index
    @hotels = Hotel.all
  end

  def new
    @hotel = Hotel.new
  end

  def show
    @hotel = Hotel.find(params[:id])
  end


  def create
    @hotel = Hotel.new(hotel_params)

    if @hotel.save
      redirect_to '/'
    else
      @errors = @hotel.errors.full_messages
      render :new
    end
  end

  private

  def hotel_params
    params.require(:hotel).permit(:name, :description, :stars, :image)
  end
end
