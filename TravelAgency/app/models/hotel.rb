class Hotel < ApplicationRecord
  has_many :packages
  has_one_attached :image


  validates :name, presence: true 
  validates :description, presence: true 
  validates :stars, presence: true 

end
