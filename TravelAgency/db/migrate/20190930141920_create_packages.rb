class CreatePackages < ActiveRecord::Migration[6.0]
  def change
    create_table :packages do |t|
      t.string :destination, null: false
      t.integer :final_price, null: false
      t.integer :departure_date, null: false
      t.integer :arrival_date, null: false

      t.references :hotels

      t.timestamps
    end

    add_index :packages, :hotel_id
  end
end
